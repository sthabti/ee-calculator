import React from "react";
import ReactDOM from "react-dom";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import { reset } from "styled-reset";
import { Calculator } from "./pages";
import theme from "./theme";
const Style = createGlobalStyle`
  html {
    box-sizing: border-box;
    height: 100%;
  }

  html, 
  body {
       min-height: 100%;
}
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }

  body {
    margin: 0;
    padding: 0;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
      "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  ${reset}
`;

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <>
      <Style />
      <Calculator />
    </>
  </ThemeProvider>,
  document.getElementById("root")
);
