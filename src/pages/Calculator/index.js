import React, { Component } from "react";
import styled, { css } from "styled-components";
import { Result, KeyPad } from "../../components";

const performCalculation = {
  "÷": (firstOperand, secondOperand) => firstOperand / secondOperand,

  "×": (firstOperand, secondOperand) => firstOperand * secondOperand,

  "+": (firstOperand, secondOperand) => firstOperand + secondOperand,

  "-": (firstOperand, secondOperand) => firstOperand - secondOperand
};

class Calculator extends Component {
  state = {
    firstOperand: "",
    secondOperand: "",
    operator: "",
    calculation: "0"
  };



  setNumber = number => {
    this.setState(state => {
      if (!state.operator) {
        const firstOperand = `${state.firstOperand}${number}`
        return {
          firstOperand: firstOperand,
          calculation: this._buildCalculationResult({
            firstOperand
          })
        };
      }

      if (state.firstOperand && state.operator) {
        const operator = `${state.secondOperand}${number}`
        return {
          secondOperand: operator,
          calculation: this._buildCalculationResult({
            secondOperand: operator
          })
        };
      }
    });
  };

  setOperator = operator => {
    this.setState(state => {
      if (state.secondOperand) {
        const calculation = this._getCalculation(state);
        return {
          calculation,
          firstOperand: calculation,
          secondOperand: "",
          operator
        };
      }

      return {
        operator,
        calculation: this._buildCalculationResult({ operator })
      };
    });
  };

  equals = () => {
    this.setState(state => ({
      calculation: this._getCalculation(state)
    }));
  };

  reset = () => {
    this.setState(() => ({
      calculation: 0,
      firstOperand: "",
      secondOperand: "",
      operator: ""
    }));
  };

  _buildCalculationResult = state => {
    const data = { ...this.state, ...state };
    const result = `${data.firstOperand} ${data.operator} ${data.secondOperand}`;
    return result.trim();
  };

  _getCalculation = ({ firstOperand, secondOperand, operator }) => {
    if (firstOperand && secondOperand && operator) {
      return performCalculation[operator](
        parseFloat(firstOperand),
        parseFloat(secondOperand)
      );
    }
    return "";
  };

  _calculate = ({ type, value }) => {
    switch (type) {
      case "number":
        return this.setNumber(value);
      case "operation":
        return this.setOperator(value);
      case "equals":
        return this.equals();
      case "decimal":
        return this.setDecimal();
      case "reset":
        return this.reset();
      default:
        return this.reset();
    }
  };

  render() {
    return (
      <Container>
        <EEImage
          src={"https://i.imgur.com/JYsWyhs.png"}
          alt="logo"
          height="100"
        />
        <CalculatorWrapper>
          <Result value={this.state.calculation} />
          <KeyPad dispatch={this._calculate} />
        </CalculatorWrapper>
      </Container>
    );
  }
}

const EEImage = styled.img`
  background-color: ${props => props.theme.eeBackground};
  border-right: 1px dashed black;
`;

const CalculatorWrapper = styled.div`
  background: ${props => props.theme.calculatorBg};
  height: 400px;
  display: flex;
  border-radius: ${props => props.theme.borderRadius};
  display: flex;
  flex-direction: column;
`;

const gradient = css`
  ${props => `linear-gradient(
           150deg, 
         ${props.theme.backgroundOne} 0%, 
         ${props.theme.backgroundOne} 20%, 
         ${props.theme.backgroundTwo} 52%,
         ${props.theme.backgroundTwo} 100%
)`}
`;

const Container = styled.div`
  background: ${gradient};
  height: 100%;
  min-height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Calculator;
