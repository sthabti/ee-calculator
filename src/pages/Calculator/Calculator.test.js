import React from "react";
import { mount } from "enzyme";
import Calculator from './index';
import { Result, KeyPad } from '../../components'
const spy = jest.fn();

let component = null;


afterEach(() => {
    spy.mockReset();
    component = null;
});

beforeEach(() => {
    component = mount(<Calculator />)
})

it('should render', () => {
    expect(component).toMatchSnapshot();
});


it('setNumber() - should update firstOperand', () => {
    const setNumberSpy = jest.spyOn(component.instance(), 'setNumber');

    component.find('.keypad').at(2).simulate('click');

    expect(setNumberSpy).toHaveBeenCalledWith(7);
    expect(component.state().firstOperand).toBe("7");
    expect(component.state().calculation).toBe("7");
});

it('setNumber() - should set secondOperand state if operator is set', () => {
    const setNumberSpy = jest.spyOn(component.instance(), 'setNumber');

    component.setState({
        firstOperand: '7',
        operator: '+'
    });

    component.find('.keypad').at(6).simulate('click'); // 9

    expect(setNumberSpy).toHaveBeenCalledWith(9);
    expect(component.state().secondOperand).toBe("9");
    expect(component.state().calculation).toBe("7 + 9");
});

it('setOperator() - should set operate state', () => {
    const setOperatorSpy = jest.spyOn(component.instance(), 'setOperator');

    component.find('.keypad-1').at(2).simulate('click'); // 8
    component.find('.keypad-14').at(0).simulate('click'); // ÷

    expect(setOperatorSpy).toHaveBeenCalledWith("÷");
    expect(component.state().operator).toBe("÷");
    expect(component.state().calculation).toBe("8 ÷");

});

it('setOperator() - should calculate result when called after secondOperand', () => {
    const setOperatorSpy = jest.spyOn(component.instance(), 'setOperator');

    component.setState({
        firstOperand: '7',
        operator: '+',
        secondOperand: '2'
    });

    component.find('.keypad-14').at(0).simulate('click'); // ÷

    expect(component.state().calculation).toBe(9)
    expect(component.state().firstOperand).toBe(9)
    expect(component.state().secondOperand).toBe("")
    expect(component.state().operator).toBe("÷")

});

it('equals() - should display result from state', () => {

    component.setState({
        firstOperand: '7',
        operator: '+',
        secondOperand: '3'
    });

    component.instance().equals();
    expect(component.state().calculation).toBe(10);

    component.setState({
        firstOperand: '7',
        operator: '-',
        secondOperand: '3'
    });

    component.instance().equals();

    expect(component.state().calculation).toBe(4);

});

it('reset() - should reset state', () => {
    component.setState({
        firstOperand: '7',
        operator: '+',
        secondOperand: '3'
    });

    expect(component.state().firstOperand).toBe('7');

    component.instance().reset();

    expect(component.state().firstOperand).toBe('');
    expect(component.state().secondOperand).toBe('');
    expect(component.state().calculation).toBe(0);

})
