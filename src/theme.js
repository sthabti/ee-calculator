const theme = {
  backgroundOne: "#F09B9D",
  backgroundTwo: "#D867A3",
  eeBackground: "#FFFFFF",
  borderRadius: "10px",
  calculatorBg: "#364150",
  resultBg: "#495362",
  buttonOperatorBg: "#F1F1F1"
};

export default theme;
