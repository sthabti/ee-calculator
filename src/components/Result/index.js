import React from "react";
import type from "prop-types";

import styled, { css } from "styled-components";

const Result = ({ value }) => {
  return (
    <ResultContainer>
      <span>{value}</span>
    </ResultContainer>
  );
};

Result.propTypes = {

  value: type.oneOfType([type.string, type.number]).isRequired

}

export default Result;

const gradientResult = css`
  ${props => `linear-gradient(90deg, #505A69 20%, #394453 72%
)`}
`;

const ResultContainer = styled.header`
  background: ${gradientResult};
  height: 150px;
  display: flex;
  width: 100%;
  border-top-left-radius: ${props => props.theme.borderRadius};
  border-top-right-radius: ${props => props.theme.borderRadius};
  box-shadow: inset 0px -10px 33px 1px #394453;
  padding: 10px;
  color: white;
  justify-content: flex-end;
  align-items: flex-end;

  & span {
    font-size: 3rem;
  }
`;
