import React from "react";
import { shallow } from "enzyme";
import Result from "./index.js";

const result = '10'

const component = shallow(<Result value={result} />);

it("should render value", () => {
  expect(component).toMatchSnapshot();
  expect(component.find('span').text()).toBe(result);
});
