import React from "react";
import { mount } from "enzyme";
import KeyPad, { Button } from "./index.js";
import keys from "./keys.json";
const spy = jest.fn();
const component = mount(<KeyPad dispatch={spy} />);

afterEach(() => {
  spy.mockReset();
});

it("should render", () => {
  expect(component).toMatchSnapshot();
});

it("should have correct number of keys", () => {
  expect(component.find(Button).length).toBe(keys.length);

  expect(
    component
      .find(Button)
      .at(1)
      .text()
  ).toBe(String(keys[1].value));
});

it("clicking a key should fire a function with correct parameters", () => {
  component
    .find("button")
    .at(11)
    .simulate("click");

  expect(spy).toHaveBeenCalledWith({ type: "operation", value: "-" });
});
