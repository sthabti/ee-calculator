import React from "react";
import type from "prop-types";
import styled from "styled-components";
import keys from "./keys.json";
const KeyPad = ({ dispatch }) => {
  return (
    <Container className="keypad-container">
      {keys.map((item, index) => (
        <Button
          className={`keypad keypad-${index}`}
          key={item.value}
          onClick={() => dispatch({ type: item.type, value: item.value })}
          type={item.type}
        >
          {item.value}
        </Button>
      ))}
    </Container>
  );
};

KeyPad.propTypes = {
  dispatch: type.func.isRequired
}

export default KeyPad;

const Container = styled.section`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  align-content: center;
  align-items: flex-start;
  background: #ffffff;
`;

export const Button = styled.button`
  display: flex;
  flex: 0 1 25%;
  justify-content: center;
  align-items: center;
  padding: 1px;
  height: 50px;
  cursor: pointer;
  font-size: 2em;
  border: none;
  background: ${props =>
    props.type !== "number" ? props.theme.buttonOperatorBg : ""};
`;
